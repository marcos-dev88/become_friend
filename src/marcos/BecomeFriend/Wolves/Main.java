package marcos.BecomeFriend.Wolves;

import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import marcos.BecomeFriend.EventRegister.EventRegister;

public class Main extends JavaPlugin{
	
	PluginManager pm = (PluginManager) this.getServer().getPluginManager();
	
	@Override
	public void onLoad() {
		this.getServer().broadcastMessage(ChatColor.WHITE +"===================");
		this.getServer().broadcastMessage(ChatColor.DARK_AQUA +"-Become Friends-");
		this.getServer().broadcastMessage(ChatColor.WHITE +"===================");
		
		super.onLoad();
	}
	
	@Override
	public void onEnable() {
		 pm.registerEvents(new EventRegister(), this);
		 
		super.onEnable();
	}
	
	@Override
	public void onDisable() {
		HandlerList.unregisterAll();
		
		super.onDisable();
	}

}
