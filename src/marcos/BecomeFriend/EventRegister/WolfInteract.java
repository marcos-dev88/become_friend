package marcos.BecomeFriend.EventRegister;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.inventory.ItemStack;

public class WolfInteract {

	public ItemStack porkBeef() {

		ItemStack beef = new ItemStack(Material.PORK);

		return beef;
	}

	@SuppressWarnings("deprecation")
	public void Interact(Player player, Entity wolfInteract) {

		if (wolfInteract instanceof Wolf) {
			Wolf wolf = (Wolf) wolfInteract;
			if (player.getItemInHand().isSimilar(porkBeef())) {
				if (wolf.getType().equals(EntityType.WOLF)) {
					if (!wolf.isTamed()) {
						int rndom = new Random().nextInt(7);
						if (rndom >= 5 && wolf.isAngry()) {
							Entity e = player.getWorld().spawnEntity(wolf.getLocation(), EntityType.BAT);
							e.setCustomName("");
							wolf.setTarget((LivingEntity) e);
							wolf.setAngry(false);
							e.remove();
							player.sendMessage(ChatColor.BLUE + "You are friends now!");
						} else if (wolf.isTamed() && wolf.isAngry()) {
							player.sendMessage(
									ChatColor.DARK_RED + "It won't be your friend, because it already has a owner.");
						}
						if (wolf.isAngry() && !wolf.isTamed()) {
							player.getInventory().getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
						}
					}
				}
			}
		}
	}
}
