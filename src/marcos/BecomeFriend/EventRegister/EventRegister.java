package marcos.BecomeFriend.EventRegister;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class EventRegister implements Listener {

	@EventHandler
	public void PlayerInteract(PlayerInteractEntityEvent event) {

		WolfInteract wolf = new WolfInteract();
		Player player = event.getPlayer();
		Entity wolfInteragido = event.getRightClicked();
		wolf.Interact(player, wolfInteragido);

	}

}
